#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

perl -e 'printf "\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0075b1e8)) bs=1 count=4

#This second patch is commented out because of the comments in the end
#perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b1ec)) bs=1 count=4
#This is equivalent to one statement
#perl -e 'printf "\x00\x00\x00\x03\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b1e8)) bs=1 count=8

#How to disable?
#perl -e 'printf "\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b1e8)) bs=1 count=4

#To display to result
#dd if=/dev/ps3ram skip=$((0x0075b1e8)) bs=1 count=8 | hexdump -C

#comments
#This was the result prior to apply the patch
#dd if=/dev/ps3ram skip=$((0x0075b1e8)) bs=1 count=8 | hexdump -C
#8+0 records in
#8+0 records out
#00000000  00 00 00 00 00 00 00 01                           |........|
#00000008
#8 bytes (8 B) copied, 0.000229226 s, 34.9 kB/s
#Notice there is 1 in the end already
#So I propose only first statement is necessary:
#perl -e 'printf "\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0075b1e8)) bs=1 count=4
#val = lv1_peek(0x0075b1e8);
#lv1_poke(0x0075b1e8, ((val & 0xFFFFFFFF) | 0x0000000300000000UUL));
#End of comments
