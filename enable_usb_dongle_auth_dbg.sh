#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0075b070)) bs=1 count=8
# lv1_poke(0x0075b070, 0x03);
perl -e 'printf "\x01"' | dd of=/dev/ps3ram seek=$((0x0075b06E)) bs=1 count=1
# val = lv1_peek(0x0075b06E);
# lv1_poke(0x0075b06E, ((val & 0x00FFFFFFFFFFFFFFUUL) | 0x0100000000000000UUL));

#How to disable?
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b070)) bs=1 count=8
#perl -e 'printf "\x00"' | dd of=/dev/ps3ram seek=$((0x0075b06E)) bs=1 count=1

#To display
# dd if=/dev/ps3ram skip=$((0x0075b070)) bs=1 count=8 | hexdump -C
# dd if=/dev/ps3ram skip=$((0x0075b06E)) bs=1 count=8 | hexdump -C

# comments
# notice in both offsets, the original value there were zeroes.
# so to disable, just simply set the value back to zero again
# end comments
