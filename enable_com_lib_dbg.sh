#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0075b560)) bs=1 count=8
#lv2_poke(0x0075b560, 0x0000000000000003UUL);

#I've commented the following out since its value is already the desired value (see comments below)
#perl -e 'printf "\x01"' | dd of=/dev/ps3ram seek=$((0x0075b55c)) bs=1 count=1
#val =  lv1_peek(0x75b55c);
#lv1_poke(0x75b55c, ((val & 0x00FFFFFFFFFFFFFFFFULL) | 0x010000000000000000ULL));

#How to disable?
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b560)) bs=1 count=8
#perl -e 'printf "\x01"' | dd of=/dev/ps3ram seek=$((0x0075b55c)) bs=1 count=1

#To display
#dd if=/dev/ps3ram skip=$((0x0075b560)) bs=1 count=8 | hexdump -C
#dd if=/dev/ps3ram skip=$((0x0075b55c)) bs=1 count=8 | hexdump -C

#Comments
#at 0x0075b55c prior to apply the patch
# dd if=/dev/ps3ram skip=$((0x0075b55c)) bs=1 count=8 | hexdump -C
#8+0 records in
#8+0 records out
#00000000  01 00 00 00 00 00 00 00                           |........|
#00000008
#8 bytes (8 B) copied, 0.000228199 s, 35.1 kB/s
#End of comments
