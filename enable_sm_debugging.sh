#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

perl -e 'printf "\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0075b2c0)) bs=1 count=4
#GameOS patching statement using lv1_peek and lv1_poke
#val = lv1_peek(0x75b2c0);
#lv1_poke(0x75b2c0, ((val & 0xFFFFFFFF) | 0x0000000300000000UUL));
#enable printfk
perl -e 'printf "\x00\x00\x00\x01\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4b0)) bs=1 count=8
#the following two lines are equivalent to the above statement
#perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4b0)) bs=1 count=4
#perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4b4)) bs=1 count=4
#GameOS patching statement using lv1_peek and lv1_poke
#lv1_poke(0x75b4b0, 0x0000000100000001ULL);

#How to disable?
#perl -e 'printf "\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b2c0)) bs=1 count=4
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b4b0)) bs=1 count=8

#display
#dd if=/dev/ps3ram skip=$((0x0075b2c0)) bs=1 count=8 | hexdump -C
#dd if=/dev/ps3ram skip=$((0x0075b4b0)) bs=1 count=8 | hexdump -C
