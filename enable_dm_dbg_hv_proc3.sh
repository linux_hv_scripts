#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

#I commented out the first patch as the value is already at the desired level (See comments in the end)
#perl -e 'printf "\x01"' | dd of=/dev/ps3ram seek=$((0x0008d8b8)) bs=1 count=1
#val = lv1_peek(0x8d8b8);
#lv1_poke(0x8d8b8, ((0x00FFFFFFFFFFFFFFULL & val) | 0x0100000000000000ULL));

perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0008d8c0)) bs=1 count=8
#lv1_poke(0x8d8c0, 0x0000000000000003ULL);

#How to disable
#perl -e 'printf "\x00"' | dd of=/dev/ps3ram seek=$((0x0008d8b8)) bs=1 count=1
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0008d8c0)) bs=1 count=8

#To display
#dd if=/dev/ps3ram skip=$((0x0008d8b8)) bs=1 count=8 | hexdump -C
#dd if=/dev/ps3ram skip=$((0x0008d8c0)) bs=1 count=8 | hexdump -C

#Comments
#at 0x0008d8b8, prior to the patch, the value is at the desired level
#dd if=/dev/ps3ram skip=$((0x0008d8b8)) bs=1 count=8 | hexdump -C
#8+0 records in
#8+0 records out
#00000000  01 00 00 00 00 00 00 00                           |........|
#00000008
#8 bytes (8 B) copied, 0.000232222 s, 34.4 kB/s
#End of Comments

