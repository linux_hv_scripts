#!/bin/bash
#
#    dmpatch.sh
#
#    PS3 DM patch for 3.41-3.55
#    thanks graf chokoloko :)
#    inital version

# Don't replace LAID field in SS header, you need it to decrypt USB Dongle Master Key and for other crypto stuff
perl -e 'printf "\x60\x00\x00\x00"' | dd of=/dev/ps3ram bs=1 seek=$((0x16f3bc))
# Disable SPM (Security Policy Manager) check in DM
perl -e 'printf "\x3B\xE0\x00\x01"' | dd of=/dev/ps3ram bs=1 seek=$((0x16f458))
perl -e 'printf "\x38\x60\x00\x00"' | dd of=/dev/ps3ram bs=1 seek=$((0x16f460))
# enable check_int in ps3dm_um and get_version in ps3dm_sm, and enable all services even include those not enabled at all for GameOS
perl -e 'printf "\x38\x60\x00\x01"' | dd of=/dev/ps3ram bs=1 seek=$((0x16f3e4))
