#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

# Setting ss.common.debug.level

perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4bc)) bs=1 count=4
# val = lv1_peek(0x0075b4bc);
# lv1_poke(0x0075b4bc, ((val & 0xFFFFFFFF) | 0x0000000100000000UUL));
#perl -e 'printf "\x00\x00\x00\x01\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4b8)) bs=1 count=8
#lv1_poke(0x0075b4b8, 0x0000000100000001UUL);
#perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4b8)) bs=1 count=4
#perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b4bc)) bs=1 count=4
#dd if=/dev/ps3ram skip=$((0x0075b4b8)) bs=1 count=8 | hexdump -C

# Setting ss.sb_mngr.debug.level

perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x0075b478)) bs=1 count=4
#val = lv1_peek(0x0075b478);
#lv1_poke(0x0075b478, ( (val & 0xFFFFFFFF) | 0x0000000100000000UUL))
perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x03"' | dd of=/dev/ps3ram seek=$((0x0075b480)) bs=1 count=8
#lv1_poke(0x0075b480, 0x0000000000000003UUL);

#how to disable
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b4b8)) bs=1 count=8
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x0075b480)) bs=1 count=8
#

#display
#dd if=/dev/ps3ram skip=$((0x0075b478)) bs=1 count=8 | hexdump -C
#dd if=/dev/ps3ram skip=$((0x0075b480)) bs=1 count=8 | hexdump -C
#dd if=/dev/ps3ram skip=$((0x0075b478)) bs=1 count=8 | hexdump -C (this is relady the required value)
#dd if=/dev/ps3ram skip=$((0x0075b480)) bs=1 count=8 | hexdump -C

#Comments
#Notice it's already \x00\x00\x00\x01 when do the following:
#dd if=/dev/ps3ram skip=$((0x0075b478)) bs=1 count=8 | hexdump -C
#8+0 records in
#8+0 records out
#00000000  00 00 00 01 00 00 00 00                           |........|
#00000008
#8 bytes (8 B) copied, 0.000231394 s, 34.6 kB/s
#similarly at 0x0075b480
#dd if=/dev/ps3ram skip=$((0x0075b480)) bs=1 count=8 | hexdump -C
#8+0 records in
#8+0 records out
#00000000  00 00 00 00 00 00 00 00                           |........|
#00000008
#8 bytes (8 B) copied, 0.000229966 s, 34.8 kB/s
#End of Comments

