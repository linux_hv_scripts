#!/bin/bash
# /********************************************************
# Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>
# This is free software.  You may redistribute copies of it
# under the terms of
# the GNU General Public License 2
# <http://www.gnu.org/licenses/gpl2.html>.
# There is NO WARRANTY, to the extent permitted by law.
#
# This script contains comments on how to use lv1_peek and lv1_poke in GameOS to achieve the same effect
# Please use it at your own discretion.
# credit goes to a.user (a.user@bk.ru)
#
# It also contains comments on how to disable the patch
# This script has been tested on 3.55 Kmeaw CFW
# ******************************************************/

perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e70)) bs=1 count=4
perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e74)) bs=1 count=4
perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e78)) bs=1 count=4
perl -e 'printf "\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e7c)) bs=1 count=4
#could be written as
#perl -e 'printf "\x00\x00\x00\x01\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e70)) bs=1 count=8
#perl -e 'printf "\x00\x00\x00\x01\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e78)) bs=1 count=8
#or as one block
#perl -e 'printf "\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e70)) bs=1 count=16
#lv1_poke(0xe0e70, 0x0000000100000001ULL);
#lv1_poke(0xe0e78, 0x0000000100000001ULL);

#How to disable? (see comments in the end)
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x00"' | dd of=/dev/ps3ram seek=$((0x000e0e70)) bs=1 count=8
#perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x01"' | dd of=/dev/ps3ram seek=$((0x000e0e78)) bs=1 count=8

#To display
#dd if=/dev/ps3ram skip=$((0x000e0e70)) bs=1 count=8 | hexdump -C
#dd if=/dev/ps3ram skip=$((0x000e0e78)) bs=1 count=8 | hexdump -C

#Comments
#At 0x000e0e7c, it's already at the desired value
# dd if=/dev/ps3ram skip=$((0x000e0e78)) bs=1 count=8 | hexdump -C
#8+0 records in
#8+0 records out
#00000000  00 00 00 00 00 00 00 01                           |........|
#00000008
#8 bytes (8 B) copied, 0.000228025 s, 35.1 kB/s
#End of Comments
